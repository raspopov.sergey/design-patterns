﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DesignPatterns._04_Prototype
{
    public class Excersise
    {
        public static void Run()
        {

        }
    }

    public class Point
    {
        public int X, Y;

        public Point(Point point)
        {
            X = point.X;
            Y = point.Y;
        }


    }

    public class Line
    {
        public Point Start, End;

        public Line DeepCopy()
        {
            return new Line()
            {
                Start = new Point(Start),
                End = new Point(End)
            };
        }
    }
}
