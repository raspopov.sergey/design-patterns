﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DesignPatterns._04_Prototype
{
    public class Prototype
    {
        public static void Run()
        {
            var john = new Person(new[] { "John", "Smith" },
                new Address("Kal Lake rd.", 111));
            var jane = john.DeepCopy();
            jane.Names[0] = "Jane";
            jane.Address.HouseNumber = 333;

            Console.WriteLine(john);
            Console.WriteLine(jane);
        }
    }

    public static class ExtensionsMethods
    {
        public static T DeepCopy<T>(this T self)
        {
            using (var ms = new MemoryStream())
            {
                var formater = new XmlSerializer(typeof(T));
                formater.Serialize(ms, self);
                ms.Position = 0;
                return (T)formater.Deserialize(ms);
            }
        }
    }

    public class Person
    {
        public Person()
        {

        }
        public Person(string[] names, Address address)
        {
            Names = names;
            Address = address;
        }

        public string[] Names { get; set; }
        public Address Address { get; set; }

        public override string ToString()
        {
            return $"{string.Join(" ", Names)} lives at {Address}";
        }
    }

    public class Address
    {
        public Address()
        {

        }
        public Address(string street, int houseNumber)
        {
            Street = street;
            HouseNumber = houseNumber;
        }

        public string Street { get; set; }
        public int HouseNumber { get; set; }

        public override string ToString()
        {
            return $"{HouseNumber} {Street}";
        }
    }
}
