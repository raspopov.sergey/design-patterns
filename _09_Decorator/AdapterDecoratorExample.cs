﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._09_Decorator
{
    public static class AdapterDecoratorExample
    {
        public static void Run()
        {
            MyStringBuilder s = "hello ";
            s += "world";
            Console.WriteLine(s);
        }
    }

    public class MyStringBuilder
    {
        private StringBuilder _builder;

        public MyStringBuilder(StringBuilder builder)
        {
            _builder = builder;
        }

        public static implicit operator MyStringBuilder(string text)
        {
            return new MyStringBuilder(new StringBuilder(text));
        }

        public override string ToString()
        {
            return _builder.ToString();
        }
    }
}
