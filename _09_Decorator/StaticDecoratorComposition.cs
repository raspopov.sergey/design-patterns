﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._09_Decorator.StaticDecoratorComposition
{
    [Obsolete("This pattern doesn't work in C# because constructor forwarding is not available.")]
    public class StaticDecoratorComposition
    {
        public static void Run()
        {
            //in this case it is impossible to access red square side property.
            var redCircle = new ColoredShape<Circle>("red");
            Console.WriteLine(redCircle.AsString());
        }
    }

    public abstract class Shape
    {
        public abstract string AsString();
    }

    public class Circle : Shape
    {
        private float _radius;

        public Circle() : this(1f)
        {

        }

        public Circle(float radius)
        {
            _radius = radius;
        }

        public override string AsString() => $"Circle with {_radius} radius";
    }

    public class ColoredShape<T> : Shape where T : Shape, new()
    {
        private T _shape = new T();
        private string _color;

        public ColoredShape() : this("black")
        {

        }

        public ColoredShape(string color)
        {
            _color = color;
        }

        public override string AsString() => $"{_shape.AsString()} has {_color} color";
    }

    public class TransparentShape<T> : Shape where T : Shape, new()
    {
        private Shape _shape;
        private float _transparency;

        public TransparentShape(Shape shape, float transparency)
        {
            _shape = shape;
            _transparency = transparency;
        }

        public override string AsString() =>
            $"{_shape.AsString()} is {string.Format("{0:P}", _transparency)} transparent";
    }
}
