﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._09_Decorator
{
    public class MultipleInheritanceExample
    {
        public static void Run()
        {
            var b = new Bird();
            var l = new Lizard();
            var d = new Dragon(b, l);
            d.Weight = 123;
            d.Fly();
            d.Crawl();
        }
    }

    public interface IFlyable
    {
        void Fly();
    }

    public interface ICrawlable
    {
        void Crawl();
    }

    public class Bird : IFlyable
    {
        public int Weight { get; set; }
        public void Fly()
        {
            Console.WriteLine($"soaring in the sky weighing {Weight}.");
        }
    }

    public class Lizard : ICrawlable
    {
        public int Weight { get; set; }
        public void Crawl()
        {
            Console.WriteLine($"crawling in the dirt weighing {Weight}.");
        }
    }

    public class Dragon : IFlyable, ICrawlable
    {
        private int _weight;
        private Bird _bird;
        private Lizard _lizard;

        public Dragon(Bird bird, Lizard lizard)
        {
            _bird = bird;
            _lizard = lizard;
        }

        public int Weight
        {
            get { return _weight; }
            set {
                _weight = value;
                _bird.Weight = value;
                _lizard.Weight = value;
            }
        }

        public void Crawl()
        {
            _lizard.Crawl();
        }

        public void Fly()
        {
            _bird.Fly();
        }
    }
}
