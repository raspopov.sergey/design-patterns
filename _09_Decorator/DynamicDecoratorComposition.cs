﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._09_Decorator.DynamicDecoratorComposition
{
    public class DynamicDecoratorComposition
    {
        public static void Run()
        {
            var square = new Square(1.23f);
            Console.WriteLine(square.AsString());

            var redSquare = new ColoredShape(square, "red");
            Console.WriteLine(redSquare.AsString());

            var redTransparentSquare = new TransparentShape(redSquare, 0.5f);
            Console.WriteLine(redTransparentSquare.AsString());
        }
    }

    public interface IShape
    {
        string AsString();
    }

    public class Circle : IShape
    {
        private float _radius;

        public Circle(float radius)
        {
            _radius = radius;
        }

        public string AsString()
        {
            return $"Circle with radius {_radius}";
        }

        public void Resize(float factor)
        {
            _radius *= factor;
        }
    }

    public class Square : IShape
    {
        private float _side;

        public Square(float side)
        {
            _side = side;
        }

        public string AsString()
        {
            return $"Square with side {_side}";
        }

        public void Resize(float factor)
        {
            _side *= factor;
        }
    }

    public class ColoredShape : IShape
    {
        private IShape _shape;
        private string _color;

        public ColoredShape(IShape shape, string color)
        {
            _shape = shape;
            _color = color;
        }

        public string AsString() => $"{_shape.AsString()} has color {_color}";
    }

    public class TransparentShape : IShape
    {
        private IShape _shape;
        private float _transparency;

        public TransparentShape(IShape shape, float transparency)
        {
            _shape = shape;
            _transparency = transparency;
        }

        public string AsString() => $"{_shape.AsString()} is {string.Format("{0:p}", _transparency)} transparent";
    }
}
