﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._09_Decorator
{
    public static class Runner
    {
        public static void Run()
        {
            var cb = new CodeBuilder();
            cb.AppendLine("public class Foo")
                .AppendLine("{")
                .AppendLine("}");

            Console.WriteLine(cb);
        }
    }

    public class CodeBuilder //: StringBuilder 
    {//can't inherit from StringBuilder because it is sealed, use decorator instead
        private StringBuilder _builder = new StringBuilder();
        //only implementing AppendLine and ToString 
        public CodeBuilder AppendLine(string text)
        {
            _builder.AppendLine(text);
            return this;
        }

        public override string ToString()
        {
            return _builder.ToString();
        }
    }
}
