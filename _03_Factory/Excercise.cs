﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._03_Factory
{
    public class Excercise
    {
        public static void Run()
        {
            var pf = new Person.PersonFactory();
            var p = pf.CreatePerson("Sergey");
        }
        
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        private Person(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public class PersonFactory
        {
            private int _counter = 0;

            public Person CreatePerson(string name)
            {
                return new Person(_counter++, name);
            }
        }
    }
}
