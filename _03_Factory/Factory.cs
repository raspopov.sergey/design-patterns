﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._03_Factory
{
    public class Factory
    {
        public static void Run()
        {
            var cpoint = Point.Factory.NewCartesianPoint(1, 1);
            var ppoint = Point.Factory.NewPolarPoint(13, 0.2);

            Console.WriteLine("Cartesian:");
            Console.WriteLine(cpoint);
            Console.WriteLine("Polar:");
            Console.WriteLine(ppoint);
        }
        
    }

    public class Point
    {
        private double x, y;

        private Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return $"X: {x}; Y:{y}";
        }

        public static class Factory
        {
            public static Point NewCartesianPoint(double x, double y)
            {
                return new Point(x, y);
            }

            public static Point NewPolarPoint(double rho, double theta)
            {
                return new Point(rho * Math.Cos(theta), rho * Math.Sin(theta));
            }
        }
    }
}
