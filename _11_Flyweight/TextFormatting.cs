﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._11_Flyweight
{
    public class TextFormatting
    {
        public static void Run()
        {
            var str = "This is a brave new world";
            var ft = new FormattedText(str);
            ft.GetRange(10, 15).Capitalize = true;
            Console.WriteLine(ft);
        }
    }

    public class FormattedText
    {
        private string _plainText;
        private List<TextRange> _formatting = new List<TextRange>();

        public FormattedText(string plainText)
        {
            _plainText = plainText;
        }
        
        public TextRange GetRange(int start, int end)
        {
            var range = new TextRange() { Start = start, End = end };
            _formatting.Add(range);
            return range;
        }
        
        public class TextRange
        {
            public int Start, End;
            public bool Capitalize, Bold, Italic;

            public bool Convers(int position)
            {
                return position >= Start && position <= End;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < _plainText.Length; i++)
                foreach (var f in _formatting)
                    if (f.Convers(i) && f.Capitalize)
                        sb.Append(Char.ToUpper(_plainText[i]));
                    else
                        sb.Append(_plainText[i]);

            return sb.ToString();
        }
    }

    
}
