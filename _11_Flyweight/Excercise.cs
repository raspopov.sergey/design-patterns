﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._11_Flyweight
{
    public class Excercise
    {
        public static void Run()
        {
            var sentence = new Sentence("hello world");
            sentence[1].Capitalize = true;
            Console.WriteLine(sentence.ToString());
        }
    }

    public class Sentence
    {
        private string[] _words;
        private Dictionary<int, WordToken> _tokens = new Dictionary<int, WordToken>();

        public Sentence(string plainText)
        {
            _words = plainText.Split(' ');
        }

        public WordToken this[int index]
        {
            get
            {
                var token = new WordToken();
                if (!_tokens.ContainsKey(index))
                    _tokens.Add(index, token);
                return _tokens[index];
            }
        }

        public override string ToString()
        {
            var words = new List<string>();
            for (int i = 0; i < _words.Length; i++)
                if (this[i].Capitalize)
                    words.Add(_words[i].ToUpper());
                else
                    words.Add(_words[i]);

            return string.Join(" ", words);
        }

        public class WordToken
        {
            public bool Capitalize;
        }
    }
}
