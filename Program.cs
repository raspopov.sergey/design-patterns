﻿using DesignPatterns._01_SOLID.DependencyInversion;
using DesignPatterns._01_SOLID.LiskovSubstitution;
using DesignPatterns._01_SOLID.OpenClosed;
using DesignPatterns._01_SOLID.OpenClosed.Bad;
using DesignPatterns._01_SOLID.OpenClosed.Good;
using DesignPatterns._01_SOLID.SingleResponsibility;
using System;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //SingleResponsibility();
            //OpenClosed(); // classes should be open for extension but closed for modifications
            //LiskovSubstitution(); // you should be able to substitute base type for sub type
            //InterfaceSegregation(); // don't put too much into 1 interface (split interface instead)
            //DependencyInversion(); // high level module should not depend on low level modules; use abstractions

            //_02_Builder.Builder.Run();
            //_02_Builder.FluentBuilderInheritanceWithRecursiveGenerics.Run();
            //_02_Builder.FacetedBuilder.Run();
            //_02_Builder.Excersise.Run();

            //_03_Factory.Factory.Run();
            //_03_Factory.AbstractFactory.Run();
            //_03_Factory.Excercise.Run();

            //_04_Prototype.Prototype.Run();

            //_05_Singleton.Singleton.Run();

            //_06_Adapter.Adapter.Run();

            //_07_Bridge.Bridge.Run();

            //_08_Composite.GraphicObjectsExample.Run();

            //_09_Decorator.Runner.Run();
            //_09_Decorator.AdapterDecoratorExample.Run();
            //_09_Decorator.MultipleInheritanceExample.Run();
            //_09_Decorator.DynamicDecoratorComposition.DynamicDecoratorComposition.Run();
            //_09_Decorator.StaticDecoratorComposition.StaticDecoratorComposition.Run();
            //_08_Composite.GraphicObjectsExample.Run();

            //_10_Facade - Making sure a user gets a nice interface to a more complex software
            //Example here: https://github.com/ActiveMesa/MdxConsole

            //minimize amount of data stored
            //_11_Flyweight.TextFormatting.Run();
            //_11_Flyweight.Excercise.Run();

            Console.ReadKey();
        }

        static void SingleResponsibility()
        {// separate journal handling from persistance handling
            var j = new Journal();
            j.AddEntry("First Entry");
            j.AddEntry("Second Entry");

            var p = new Persistance();
            Console.Write(j);
            p.SaveToFile(j, @"C:\temp\journal.txt", true);
        }

        static void OpenClosed()
        {
            var apple = new Product("apple", Color.Green, Size.Small);
            var tree = new Product("tree", Color.Green, Size.Large);
            var house = new Product("house", Color.Blue, Size.Large);
            Product[] products = { apple, tree, house };

            var pf = new ProductFilter();
            Console.WriteLine("Green products (old):");
            foreach (var p in pf.FilterByColor(products, Color.Green))
                Console.WriteLine($" - {p.Name} is green");

            var bf = new BetterFilter();
            Console.WriteLine("Green products (new):");
            foreach (var p in bf.Filter(products, new ColorSpecification(Color.Green)))
                Console.WriteLine($" - {p.Name} is green");

            Console.WriteLine("Large blue products:");
            foreach(var p in bf.Filter(products, new AndSpecification<Product>(
                new ColorSpecification(Color.Blue), 
                new SizeSpecification(Size.Large))))
            {
                Console.WriteLine($" - {p.Name} is blue and large");
            }
        }

        static void LiskovSubstitution()
        {
            var r = new Rectangle() { Width = 2, Height = 3 };
            Console.WriteLine($"{r} has area {r.GetArea()}");

            Rectangle s = new Square();
            s.Width = 4;
            Console.WriteLine($"{s} has area of {s.GetArea()}");
        }

        static void InterfaceSegregation()
        {

        }

        static void DependencyInversion()
        {
            var parent = new Person { Name = "John" };
            var child1 = new Person { Name = "Chris" };
            var child2 = new Person { Name = "Mary" };

            var relationships = new Relationships();//low level module
            relationships.AddParentRelationship(child1, parent);
            relationships.AddParentRelationship(child2, parent);

            Console.WriteLine("Bad:");
            new Research(relationships);

            Console.WriteLine("Good:");
            new Research(relationships);
        }
    }
}
