﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._06_Adapter
{
    public class Excercise
    {
        public static void Run()
        {

        }
    }

    public class Square
    {
        public int Side;
    }

    public interface IRectangle
    {
        int Width { get; }
        int Height { get; }
    }

    public static class ExtensionMethods
    {
        public static int Area(this IRectangle rc)
        {
            return rc.Width * rc.Height;
        }
    }

    public class SquareToRectangleAdapter : IRectangle
    {
        private int _width;
        private int _height;

        public SquareToRectangleAdapter(Square square)
        {
            _width = square.Side;
            _height = square.Side;
        }

        public int Width => _width;

        public int Height => _height;
        
    }
}
