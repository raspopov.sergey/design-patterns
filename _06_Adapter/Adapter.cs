﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._06_Adapter
{
    public class Adapter
    {
        private static List<VectorObject> objects = new List<VectorObject>()
        {
            new VectorRectangle(1, 1, 10, 10),
            new VectorRectangle(3, 3, 6, 6)
        };

        public static void Run()
        {
            Draw();
            Draw();
        }

        public static void Draw()
        {
            foreach (var vObject in objects)
                foreach (var line in vObject)
                    foreach (var point in new LineToPointAdapter(line))
                        DrawPoint(point);
        }

        public static void DrawPoint(Point p)
        {
            Console.Write(".");
        }
    }

    public class LineToPointAdapter : IEnumerable<Point>
    {
        private static int count = 0;
        private static Dictionary<int, List<Point>> cache = new Dictionary<int, List<Point>>();
        
        public LineToPointAdapter(Line line)
        {
            Console.WriteLine();
            var hash = line.GetHashCode();
            var points = new List<Point>();
            if (!cache.ContainsKey(hash))
            {
                Console.WriteLine($"{++count}: Generating points for line [{line.Start.X},{line.Start.Y}]-[{line.End.X},{line.End.Y}] (no caching)");

                var top = Math.Max(line.Start.Y, line.End.Y);
                var bottom = Math.Min(line.Start.Y, line.End.Y);
                var left = Math.Min(line.Start.X, line.End.X);
                var right = Math.Max(line.Start.X, line.End.X);
                var dx = line.End.X - line.Start.X;
                var dy = line.End.Y - line.Start.Y;
                if (dx == 0)
                    for (int i = bottom; i <= top; i++)
                        points.Add(new Point(line.Start.X, i));
                else if (dy == 0)
                    for (int i = left; i <= right; i++)
                        points.Add(new Point(i, line.Start.Y));

                cache.Add(hash, points);
            }
            else
            {
                Console.WriteLine($"{count}: Generating points for line [{line.Start.X},{line.Start.Y}]-[{line.End.X},{line.End.Y}] (caching)");
            }
        }

        public IEnumerator<Point> GetEnumerator()
        {
            return cache.Values.SelectMany(x => x).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }


    public class Point
    {
        public int X, Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"{nameof(X)}: {X}, {nameof(Y)} {Y}";
        }

        public override int GetHashCode()
        {
            return (X *397) ^ Y;
        }
    }

    public class Line
    {
        public Point Start, End;

        public Line(Point start, Point end)
        {
            Start = start ?? throw new ArgumentNullException(nameof(start));
            End = end ?? throw new ArgumentNullException(nameof(end));
        }

        public override int GetHashCode()
        {
            return (Start.GetHashCode() * 397) ^ End.GetHashCode();
        }
    }

    public class VectorObject : Collection<Line> { }

    public class VectorRectangle : VectorObject
    {
        public VectorRectangle(int x, int y, int width, int height)
        {
            var p1 = new Point(x, y);
            var p2 = new Point(x, y + height);
            var p3 = new Point(x + width, y + height);
            var p4 = new Point(x + width, y);
            Add(new Line(p1, p2));
            Add(new Line(p2, p3));
            Add(new Line(p3, p4));
            Add(new Line(p4, p1));
        }
    }
}
