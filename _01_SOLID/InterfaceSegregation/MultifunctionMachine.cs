﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.InterfaceSegregation
{
    public class MultifunctionMachine : IMultifunctionDevice
    {
        private IPrinter printer;
        private IScanner scanner;

        public MultifunctionMachine(IPrinter printer, IScanner scanner)
        {
            this.printer = printer;
            this.scanner = scanner;
        }


        //decorator pattern
        public void Print(Document doc)
        {
            printer.Print(doc);
        }

        public void Scan(Document doc)
        {
            scanner.Scan(doc);
        }
    }
}
