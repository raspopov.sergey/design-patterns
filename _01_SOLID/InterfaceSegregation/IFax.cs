﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.InterfaceSegregation
{
    public interface IFax
    {
        void Fax(Document doc);
    }
}
