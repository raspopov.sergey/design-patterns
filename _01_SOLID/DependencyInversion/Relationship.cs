﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.DependencyInversion
{
    public enum Relationship
    {
        Parent,
        Child,
        Sibling
    }

    public class Person
    {
        public string Name { get; set; }
    }

    public class Relationships : IRelationshipBrowser
    {
        private List<(Person, Relationship, Person)> relationships =
            new List<(Person, Relationship, Person)>();

        public void AddParentRelationship(Person child, Person parent)
        {
            relationships.Add((parent, Relationship.Parent, child));
            relationships.Add((child, Relationship.Child, parent));
        }

        public IEnumerable<Person> FindAllChildren(string name)
        {
            foreach (var r in relationships)
                if (r.Item1.Name == name && r.Item2 == Relationship.Parent)
                    yield return r.Item3;
        }

        public List<(Person, Relationship, Person)> Relations => relationships;
    }

    public class Research
    {
        public Research(Relationships relationships)
        {
            //find all John's children
            var relations = relationships.Relations;
            foreach (var r in relations)
                if (r.Item1.Name == "John" && r.Item2 == Relationship.Parent)
                    Console.WriteLine($"{r.Item3.Name} is John's child.");

        }

        public Research(IRelationshipBrowser browser)
        {
            foreach (var child in browser.FindAllChildren("John"))
                Console.WriteLine($"{child.Name} is John's child.");
        }
    }
    
    public interface IRelationshipBrowser
    {
        IEnumerable<Person> FindAllChildren(string name);
    }
}
