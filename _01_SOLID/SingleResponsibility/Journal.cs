﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.SingleResponsibility
{
    public class Journal
    {
        private readonly List<string> _entries = new List<string>();
        private static int count = 0;

        public int AddEntry(string entry)
        {
            _entries.Add($"{++count}: {entry}");
            return count;//memento principle
        }

        public void RemoveEntry(int index)
        {
            _entries.RemoveAt(index);
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, _entries);
        }
    }
}
