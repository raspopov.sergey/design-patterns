﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.LiskovSubstitution
{
    public class Square : Rectangle
    {
        //public new int Height { get => base.Height; set => base.Height = base.Width = value; }
        //public new int Width { get => base.Width; set => base.Width = base.Height = value; }
        public override int Height { get => base.Height; set => base.Height = base.Width = value; }
        public override int Width { get => base.Width; set => base.Width = base.Height = value; }
    }
}
