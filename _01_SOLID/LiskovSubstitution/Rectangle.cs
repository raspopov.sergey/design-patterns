﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.LiskovSubstitution
{
    public class Rectangle
    {
        //public int Width { get; set; }
        //public int Height { get; set; }
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }

        public override string ToString()
        {
            return $"{nameof(Width)}: {Width}, {nameof(Height)}: {Height}";
        }

        public int GetArea()
        {
            return Width * Height;
        }
    }
}
