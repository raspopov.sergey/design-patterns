﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.OpenClosed.Good
{
    public class BetterFilter : IProductFilter<Product>
    {
        public IEnumerable<Product> Filter(IEnumerable<Product> t, ISpecification<Product> spec)
        {
            foreach (var p in t)
                if (spec.IsSatisfied(p))
                    yield return p;
        }
    }
}
