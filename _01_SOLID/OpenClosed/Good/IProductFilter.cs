﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.OpenClosed.Good
{
    public interface IProductFilter<T>
    {
        IEnumerable<T> Filter(IEnumerable<T> t, ISpecification<T> spec);
    }
}
