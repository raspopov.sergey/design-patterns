﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._01_SOLID.OpenClosed
{
    public class Product
    {
        public string Name;
        public Color Color;
        public Size Size;

        public Product(string name, Color color, Size size)
        {
            Name = name;
            Color = color;
            Size = size;
        }
    }

    public enum Color
    {
        Green,
        Red,
        Blue
    }

    public enum Size
    {
        Small,
        Medium,
        Large
    }
}
