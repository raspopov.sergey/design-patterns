﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._05_Singleton
{
    public class Singleton
    {
        public static void Run()
        {
            
        }
    }

    public interface IDatabase
    {
        int GetPopulation(string name);
    }

    public class SingletonDatabase : IDatabase
    {
        private Dictionary<string, int> _data;
        private static int _instanceCount = 0;
        public static int Count => _instanceCount;


        private SingletonDatabase()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _data = File.ReadAllLines($"{path}/capitals.txt")
                .Batch(2)
                .ToDictionary(x => x.First().Trim(), x => int.Parse(x.ElementAt(1)));
        }

        public int GetPopulation(string name)
        {
            return _data[name];
        }

        private static Lazy<SingletonDatabase> _instance = new Lazy<SingletonDatabase>(
            () => new SingletonDatabase());

        public static SingletonDatabase Instance => _instance.Value;
    }

    public class SingletonRecordFinder
    {
        public int TotalPopulation(IEnumerable<string> names)
        {
            var total = 0;
            foreach (var name in names)
                total += SingletonDatabase.Instance.GetPopulation(name);
            return total;
        }
    }

    public class ConfigurableRecordFinder
    {
        private IDatabase _database;
        public ConfigurableRecordFinder(IDatabase database)
        {
            _database = database;
        }

        public int TotalPopulation(IEnumerable<string> names)
        {
            var total = 0;
            foreach (var name in names)
                total += _database.GetPopulation(name);

            return total;
        }
    }

    public class DummyDatabase : IDatabase
    {
        public int GetPopulation(string name)
        {
            return new Dictionary<string, int>()
            {
                ["alpha"] =  1,
                ["beta"] = 2,
                ["gamma"] = 3
            }[name];
        }
    }
}

