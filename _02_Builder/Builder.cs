﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._02_Builder
{
    public class Builder
    {
        public static void Run()
        {
            var sb = new StringBuilder();
            var words = new[] { "Hello",  "World" };

            sb.Append("<ul>");
            foreach(var word in words)
            {
                sb.AppendFormat("<li>{0}</li>", word);
            }
            sb.Append("</ul>");
            Console.WriteLine(sb);

            var builder = new HtmlBuilder("ul");
            builder.AddChild("li", "Hello");
            builder.AddChild("li", "World");
            Console.WriteLine(builder.ToString());

            builder.Clear();
            builder.AddChildFluent("li", "hello").AddChildFluent("li", "fluent world");
            Console.WriteLine(builder.ToString());
        }
    }

    public class HtmlElement
    {
        public string Name, Text;
        public List<HtmlElement> Children = new List<HtmlElement>();
        private int indentSize = 2;

        public HtmlElement()
        {

        }

        public HtmlElement(string name, string text)
        {
            Name = name;
            Text = text;
        }

        private string ToStringImpl(int indent)
        {
            var sb = new StringBuilder();
            var i = new string(' ', indent * indentSize);
            sb.AppendLine($"{i}<{Name}>");
            if (!string.IsNullOrWhiteSpace(Text))
                sb.AppendLine($"{new string(' ', indentSize * (indent + 1))}{Text}");

            foreach (var c in Children)
                sb.AppendLine(c.ToStringImpl(indent + 1));

            sb.Append($"{i}</{Name}>");

            return sb.ToString();
        }

        public override string ToString()
        {
            return ToStringImpl(0);
        }
    }

    public class HtmlBuilder
    {
        private HtmlElement root = new HtmlElement();
        private string rootName;
        public HtmlBuilder(string rootName)
        {
            this.rootName = rootName;
            root.Name = this.rootName;
        }

        public void AddChild(string name, string text)
        {
            var e = new HtmlElement(name, text);
            root.Children.Add(e);
        }

        public HtmlBuilder AddChildFluent(string name, string text)
        {
            var e = new HtmlElement(name, text);
            root.Children.Add(e);
            return this;
        }

        public override string ToString()
        {
            return root.ToString();
        }

        public void Clear()
        {
            root = new HtmlElement() { Name = rootName };
        }
    }
}
