﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._02_Builder
{
    public class FacetedBuilder
    {
        public static void Run()
        {
            Person person = new PersonBuilder()
                .Lives.At("321 Magic dr.").In("Disney City").WithPostalCode("f4g1w4")
                .Works.At("Google").As("Engineer").Earning(132000);

            Console.WriteLine(person);
        }

        public class Person
        {
            public string Address, City, PostalCode;
            public string CompanyName, Position;
            public int Salary;

            public override string ToString()
            {
                return $@"Lives at {Address} in city of {City} with postal code {PostalCode}
Works at {CompanyName} as {Position} with the salary of {Salary}";
            }
        }

        public class PersonBuilder
        {
            protected Person Person = new Person();
            public PersonJobBuilder Works => new PersonJobBuilder(Person);
            public PersonAddressBuilder Lives => new PersonAddressBuilder(Person);

            public static implicit operator Person(PersonBuilder pb)
            {
                return pb.Person;
            }
        }

        public class PersonJobBuilder : PersonBuilder
        {
            public PersonJobBuilder(Person person)
            {
                Person = person;
            }

            public PersonJobBuilder At(string companyName)
            {
                Person.CompanyName = companyName;
                return this;
            }

            public PersonJobBuilder As(string position)
            {
                Person.Position = position;
                return this;
            }

            public PersonJobBuilder Earning(int salary)
            {
                Person.Salary = salary;
                return this;
            }
        }

        public class PersonAddressBuilder : PersonBuilder
        {
            public PersonAddressBuilder(Person person)
            {
                Person = person;
            }

            public PersonAddressBuilder At(string address)
            {
                Person.Address = address;
                return this;
            }

            public PersonAddressBuilder In(string city)
            {
                Person.City = city;
                return this;
            }

            public PersonAddressBuilder WithPostalCode(string postalCode)
            {
                Person.PostalCode = postalCode;
                return this;
            }
        }
    }
}
