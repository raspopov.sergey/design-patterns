﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._02_Builder
{
    public class Excersise
    {
        public static void Run()
        {
            var cb = new CodeBuilder("Person").AddField("Name", "string").AddField("Age", "int");
            Console.WriteLine(cb);
        }
    }

    public class Person
    {
        public string Name;
        public int Age;
    }

    public class Variable
    {
        public string Name, Type;

        public Variable(string name, string type)
        {
            Name = name;
            Type = type;
        }
    }

    public class CodeBuilder
    {
        private string _name;
        private List<Variable> _variables = new List<Variable>();

        public CodeBuilder(string name)
        {
            _name = name;
        }

        public CodeBuilder AddField(string name, string type)
        {
            _variables.Add(new Variable(name, type));
            return this;
        }

        public void Clear()
        {
            _variables.Clear();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"public class {_name}");
            sb.AppendLine("{");
            foreach (var v in _variables)
                sb.AppendLine($"  public {v.Type} {v.Name};");
            sb.Append("}");
            return sb.ToString();
        }
    }
}
