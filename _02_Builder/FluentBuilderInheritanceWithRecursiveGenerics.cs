﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns._02_Builder
{
    public class FluentBuilderInheritanceWithRecursiveGenerics
    {
        public static void Run()
        {
            //var builder = new PersonJobBuilder();
            //builder.Called("Sergey"). //WorAs() is unavailable because Called() returns base class.

            var person = Person.New
                .Called("Sergey")
                .WorkAs("Software Developer")
                .WorksIn("IT")
                .Build();

            Console.WriteLine(person);
        }

        public class Person
        {
            public string Name, Position, Department;
            public class Builder : PersonDepartmentBuilder<Builder> { }
            public static Builder New => new Builder();

            public override string ToString()
            {
                return $"{Name} works as {Position} in {Department} department";
            }
        }

        //public class PersonInfoBuilder
        //{
        //    protected Person Person = new Person();

        //    public PersonInfoBuilder Called(string name)
        //    {
        //        Person.Name = name;
        //        return this;
        //    }
        //}

        ////need to add WorkAs. Using OpenClosed principal inherit the current bulder instead of modifying it
        //public class PersonJobBuilder : PersonInfoBuilder
        //{
        //    public PersonJobBuilder WorkAs(string position)
        //    {
        //        Person.Position = position;
        //        return this;
        //    }
        //}

        public abstract class PersonBuilder<SELF>
            where SELF : PersonBuilder<SELF>
        {
            protected Person Person = new Person();

            public Person Build()
            {
                return Person;
            }
        }

        public class PersonInfoBuilder<SELF> : PersonBuilder<PersonInfoBuilder<SELF>>
            where SELF : PersonInfoBuilder<SELF>
        {
            public SELF Called(string name)
            {
                Person.Name = name;
                return (SELF)this;
            }
        }

        public class PersonJobBuilder<SELF> : PersonInfoBuilder<PersonJobBuilder<SELF>>
            where SELF : PersonJobBuilder<SELF>
        {
            public SELF WorkAs(string position)
            {
                Person.Position = position;
                return (SELF)this;
            }
        }

        public class PersonDepartmentBuilder<SELF> : PersonJobBuilder<PersonDepartmentBuilder<SELF>>
            where SELF : PersonDepartmentBuilder<SELF>
        {
            public SELF WorksIn(string department)
            {
                Person.Department = department;
                return (SELF)this;
            }
        }
    }
}
